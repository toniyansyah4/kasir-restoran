
<?php 
  require_once '../core/init.php';
  // require_once '../classes/koneksi.php';
  // require_once '../koneksi.php';

  
  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  $db= new koneksi();
  $koneksi = $db->mysqli;
  $nama = $_POST['nama_user'];
 ?>
<div class="container">
  <h4 align="center">Detail Pesanan</h4>
  <h6 align="center">Nama Konsumen : <?php echo $nama ?></h6>
<table class="table" >
    <thead class="thead-light">
      <tr>
        <th>No</th>
        <th>Barang</th>
        <th>Keterangan</th>
        <th>Jumlah</th>
        <th>Harga</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;
      $id_order= $_POST['id_order'];
      $select=mysqli_query($koneksi, "SELECT * FROM detail_order c
        LEFT JOIN masakan d on d.id_masakan=c.id_masakan
        LEFT JOIN order1 e on e.id_order=c.id_order
        WHERE c.id_order = '$id_order'");
      while($data=mysqli_fetch_array($select))
      {
        ?>
        <tr>
          <td>
            <?php echo $no++; ?>
            <input type="hidden" name="id_order" value="<?php echo $_POST['id_order']; ?>">
            <input type="hidden" name="status_order" value="Sudah Bayar">
          </td>
            <td>
              <?php echo $data['nama_masakan']; ?></td>
              <td>
              <?php echo $data['keterangan']; ?></td>
              <td>
                  <?php echo $data['jumlah']; ?></td>
                  <td>
                    <?php echo "Rp. ".number_format($data['harga'], 0, ',', '.'); ?></td>
                  </tr>

                <?php } ?>
                <tr>
                  <td colspan="4">Total Harga</td>
                  <td>
                    Rp. <?php
                    $grand_total = mysqli_query($koneksi, "SELECT SUM(jumlah * harga) AS total FROM detail_order LEFT JOIN masakan ON detail_order.id_masakan = masakan.id_masakan 
                      WHERE detail_order.id_order = '$id_order'");
                    $rowGrandTotal = mysqli_fetch_array($grand_total);
                    echo number_format($rowGrandTotal['total'], 0, ',', '.'); ?>
                  </td>
                </tr>
              </tbody>
            </table>
            <a href="index.php" class="btn btn-primary">Kembali</a>
            <a href="print.php?dari=<?=$_POST['dari'];?>&sampai=<?=$_POST['sampai'];?>" class="btn btn-danger">Print</a>