<?php 
require_once '../core/init.php';

  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
$db= new koneksi();
?>
<div class="container">
	<h4 align="center">Laporan</h4>
<form method="post" action="" align="center" class="form-group">
	<div class="form-group">
		<label for="dari">Dari</label>
		<input type="date" id="dari" name='dari'/>
		<label for="sampai">Sampai</label>
		<input type="date" id="sampai" name='sampai'/>
		<br>
		<button type="submit" class="btn btn-outline-danger" name="submit">Filter Data</button>
	</div> 

	</form>
	<form class="" action="index.php?page=detail" method="post">
<table id="mytabel" align="center" class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Pelanggan</th>
      <th scope="col">No Order</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Kritik dan Saran</th>
      <th scope="col">Total</th>
      <th scope="col">Option</th>
    </tr>
  </thead>
  <tbody>
		<?php
		if(isset($_POST['submit']))
		{
			$select = mysqli_query($db->mysqli,"SELECT * FROM `transaksi` b
          LEFT JOIN user c on c.id_user=b.id_user
          LEFT JOIN order1 d on d.id_order=b.id_order
          WHERE d.status_order = 'Sudah Bayar'
          AND b.total_bayar != 0
          AND b.tanggal BETWEEN '$_POST[dari]' AND '$_POST[sampai]' ORDER by b.tanggal");
			$no  = 1;
			while($data = mysqli_fetch_array($select)) 
			{
				?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><input type="hidden" name="nama_user" value="<?php echo $data['nama_user']; ?>">
						<?php echo $data['nama_user'] ?></td>
					<td><input type="hidden" name="id_order" value="<?php echo $data['id_order']; ?>">
						<?php echo $data['id_order'] ?></td>
					<td><?php echo $data['tanggal'] ?></td>
					<td><?php echo $data['komen'] ?></td>
					<td><?php echo $data['total_bayar'] ?></td>
					<td><input type="submit" name="submit" class="btn btn-outline-primary" value="Detail"></td>
				</tr>
				<?php
			}
		}
		?>
  </tbody>
</table>
</form>
</div>