<?php 
require_once '../core/init.php';


  if ( !session::exists('username') ) {
    header('Location: login.php');
  }

  $errors = array();


  if( Input::get('submit') ) {
   
   //1. memanggil objek validasi
   $validation = new validation();

   //2. metode check
   $validation = $validation->check(array(
      'nama_user' => array(
                          'required' => true,
                          'min'      => 4,
                          'max'      => 50,
                          ),
      'username' => array(
                          'required' => true,
                          'min'      => 4,
                          'max'      => 50,
                          ),
      'password' => array(
                          'required' => true,
                          'min'      => 4,
                          ),
      'id_level' => array(
                          'required' => true,
                          )
    ));

    //3. lolos ujian
   if ( $validation->passed() ){
     $user->register_user(array(
      'nama_user' => Input::get('nama_user'),
      'username' => Input::get('username'),
      'password' => password_hash(Input::get('password'), PASSWORD_DEFAULT),
      'id_level' => Input::get('id_level')
    ));

     //session
     // session::set('username', Input::get('username'));
     header('Location: index.php');



  }else{
    $errors =  $validation->errors();
  }


  }

 ?>
 <div class="container">
  <br>
<form action="" class="form-group col-md-6 offset-sm-3" method="post">
<?php if (!empty($errors)) { ?>
 <div class="alert alert-danger col-sm-7 offset-sm-3" role="alert" id="errors">
    <?php foreach ($errors as $error) { ?>
      <li> <?php echo "$error"; ?></li>
    <?php } ?>
  </div>
<?php } ?>
  <div class="form-group">
    <label for="inputAddress">Name</label>
    <input type="text" name="nama_user" minlength="1" class="form-control" id="inputAddress" placeholder="Name">
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Username</label>
      <input type="text" name="username" minlength="1" class="form-control" id="inputEmail4" placeholder="Username">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Password</label>
      <input type="password" name="password" minlength="1" class="form-control" id="inputPassword4" placeholder="Password">
    </div>
  </div>

    <div class="form-group">
      <label for="inputState">Privilage</label>
      <select name="id_level" id="inputState" class="form-control">
        <?php
        $db = new koneksi();
        $select = mysqli_query($db->mysqli, "SELECT * FROM level WHERE id_level = 3");
        while($data = mysqli_fetch_array($select))
      {
       ?>
        <option value="<?php echo $data['id_level']; ?>"><?php echo $data['nama_level']; ?></option>
      <?php } ?>
      </select>
    </div>
  <div class="form-group">
    <div class="form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox"> Check me out
      </label>
    </div>
  </div>
  <input type="submit" name="submit" class="btn btn-primary col-md-2" value="Sign Up">
</form>
</div>
 <?php
 	include '../tools/footer.php';
 ?>