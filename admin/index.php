<?php 
  require_once '../core/init.php';


  if ( !session::exists('username') ) {
    header('Location: ../login/index.php');
  }
  require_once '../tools/header.php';
  require_once '../tools/nav_1.php';


 ?>


<ul class="nav justify-content-center">
  <li class="nav-item">
    <a class="nav-link active" href="index.php">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="index.php?page=order">Order</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="index.php?page=menu">Menu</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="index.php?page=tf">Transaksi</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="index.php?page=lapor">Laporan</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="index.php?page=register">Register</a>
  </li>
</ul>
    <?php
 $page = (isset($_GET['page']))? $_GET['page'] : "main";
 switch ($page) {
   case '': include "index.php"; break;
   case 'order': include "order.php"; break;
   case 'menu': include "menu.php"; break;
   case 'tf': include "transaksi.php"; break;
   case 'lapor': include "lapor.php"; break;
   case 'edit': include "edit.php"; break;
   case 'edit_order': include "edit_order.php"; break;
   case 'edit_user': include "edit_user.php"; break;
   case 'hapus': include "hapus.php"; break;
   case 'input': include "input.php"; break;
   case 'detail': include "detail.php"; break;
   case 'bayar': include "bayar.php"; break;
   case 'hapus_order': include "hapus_order.php"; break;

   case 'register': include "../login/register.php"; break;
    case 'main' : include "user1.php"; break;
 }
 ?>

<br>
 <?php
$db = new koneksi();
$koneksi = $db->mysqli;

  $user = $_SESSION['username'];
  //Jika akun berlevel

  $query = $koneksi->query("SELECT * FROM user WHERE username = '$user' ");
  $row = $query->fetch_array();

  if ($row['id_level'] == 3) {
    echo "<script> alert('Anda tidak memiliki hak akses');window.location='../owner/index.php';</script> ";
  }elseif ($row['id_level'] == 4) {
    echo "<script> alert('Anda tidak memiliki hak akses');window.location='../kasir/index.php';</script> ";
  } elseif ($row['id_level'] == 5) {
    echo "<script> alert('Anda tidak memiliki hak akses');window.location='../waiter/index.php';</script> ";
  } elseif ($row['id_level'] == 1) {
    echo "<script> alert('Anda tidak memiliki hak akses');window.location='../klien/index.php';</script> ";
  }
 require_once '../tools/footer.php'; ?>