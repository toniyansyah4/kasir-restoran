<?php 
  require_once '../core/init.php';
  require_once 'atas.php';
  $db= new koneksi();

  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
 ?>
 <div class="container">
 	<h4 align="center">Pegawai</h4>
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Pegawai</th>
      <th scope="col">Username</th>
      <th scope="col">Id_level</th>
      <th scope="col">Option</th>
    </tr>
  </thead>
  <tbody>
 	<?php
 		$no = 1;
        $select = mysqli_query($db->mysqli, "SELECT * FROM `user` a
        	LEFT JOIN level b on b.id_level=a.id_level
        	WHERE a.`id_level` != 1");
        while($data = mysqli_fetch_array($select))
      {
       ?>
    <tr>
      <th scope="row"><?php echo $no++ ?></th>
      <td><?php echo $data['nama_user']; ?></td>
      <td><?php echo $data['username']; ?></td>
      <td><?php echo $data['nama_level']; ?></td>
      <td><a href="index.php?page=hapus&id_user=<?php echo $data['id_user']; ?>" class="btn btn-outline-danger">Hapus</a></td>
    </tr>
      <?php } ?>
  </tbody>
</table>
</div>
