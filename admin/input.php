<?php 
  require_once '../core/init.php';
 $db= new koneksi();

  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
function tambah($db){
    
    if (isset($_POST['submit'])){
        $id = time();
        $id_paket = $_POST['id_paket'];
        $nama_masakan = $_POST['nama_masakan'];
        $harga = $_POST['harga'];
        $status_masakan = $_POST['status_masakan'];
        
        if(!empty($id_paket) && !empty($nama_masakan) && !empty($harga) && !empty($status_masakan)){
            $sql = "INSERT INTO masakan (id_masakan,id_paket, nama_masakan, harga, status_masakan) VALUES(".$id.",'".$id_paket."','".$nama_masakan."','".$harga."','".$status_masakan."')";
            $simpan = mysqli_query($db->mysqli, $sql);
            if($simpan && isset($_GET['aksi'])){
                if($_GET['aksi'] == 'create'){
                    header('location:index.php?page=input');
                }
            }
             header('location:index.php?page=menu');
        } else {
            $pesan = "Tidak dapat menyimpan, data belum lengkap!";
        }
    }
    ?>
    <form action="" class="form-group col-md-6 offset-sm-3" method="post">
    <div class="form-group">
      <label for="inputState">Paket</label>
      <select name="id_paket" id="inputState" class="form-control">
        <?php
        $select = mysqli_query($db->mysqli, "SELECT * FROM paket");
        while($data = mysqli_fetch_array($select))
      {
       ?>
        <option value="<?php echo $data['id_paket']; ?>"><?php echo $data['paket']; ?></option>
      <?php } ?>
      </select>
    </div>
  <div class="form-group">
    <label for="inputAddress">Nama Masakan</label>
    <input type="text" name="nama_masakan" minlength="1" class="form-control" id="inputAddress" placeholder="Nama Masakan">
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Harga</label>
      <input type="number" name="harga" minlength="1" class="form-control" id="inputEmail4" placeholder="Harga">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Status Masakan</label>
      <select name="status_masakan" id="inputState" class="form-control">
        <option value="Ada">Ada</option>
        <option value="Tidak Ada">Tidak Ada</option>
      </select>
    </div>
  </div>
  <input type="submit" name="submit" class="btn btn-primary col-md-2" value="Input">
  <input type="reset" name="reset" class="btn btn-primary col-md-2" value="Reset"/>
  <p><?php echo isset($pesan) ? $pesan : "" ?></p>
</form>

<?php
}
if (isset($_GET['aksi'])){
    switch($_GET['aksi']){
        case "create":
            tambah($db);
            break;
        default:
            tambah($db);
    }
} else {
    tambah($db);
}
?> 