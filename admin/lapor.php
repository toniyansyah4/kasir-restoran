<?php 
require_once '../core/init.php';
require_once 'atas.php';

  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
$db= new koneksi();
?>
<div class="container">
	<h4 align="center">Laporan Transaksi</h4>
<form method="post" action="" align="center" class="form-group">
	<div class="form-group">
		<label for="dari">Dari</label>
		<input type="date" id="dari" name='dari'/>
		<label for="sampai">Sampai</label>
		<input type="date" id="sampai" name='sampai'/>
		<br>
		<button type="submit" class="btn btn-outline-danger" name="submit">Filter Data</button>
	</div> 

	</form>
<table id="mytabel" align="center" class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Pelanggan</th>
      <th scope="col">No Order</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Pembayaran</th>
    </tr>
  </thead>
  <tbody>
		<?php
		if(isset($_POST['submit']))
		{
			$select = mysqli_query($db->mysqli,"SELECT * FROM `transaksi` b
          LEFT JOIN user c on c.id_user=b.id_user
          WHERE total_bayar != 0
          AND tanggal BETWEEN '$_POST[dari]' AND '$_POST[sampai]' ORDER by tanggal");
			$no  = 1;
			while($data = mysqli_fetch_array($select)) 
			{
				?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $data['nama_user'] ?></td>
					<td><?php echo $data['id_order'] ?></td>
					<td><?php echo $data['tanggal'] ?></td>
					<td><?php echo $data['total_bayar'] ?></td>
				</tr>
				<?php
			}
		}
		?>
  </tbody>
</table>
</div>