<?php 
  require_once '../core/init.php';
  require_once 'atas.php';


  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  $db= new koneksi();
 ?>
<div class="container">
  <h4 align="center">Order</h4>
<form action="index.php?page=order" method="post" name="clas">
  <div class="form-group" align="center">
      <label for="id_order">Nama Pelanggan</label>
      <select name="id_order" id="inputState" class="form-control">
        <?php
        $select = mysqli_query($db->mysqli, "SELECT * FROM order1 a
          LEFT JOIN user b on b.id_user=a.id_user
          ORDER by id_order
          ");
        while($data = mysqli_fetch_array($select))
      {
       ?>
        <option value="<?php echo $data['id_order']; ?>"><?php echo $data['tanggal']; ?> :
          <?php echo $data['nama_user']; ?>
        </option>
      <?php } ?>
      </select>
      <br>
      <input type="submit" name="filter" value="Filter Data" class="btn btn-success"></input>
    </div>
  </form>

    <div class="form-group">
      <table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Pelanggan</th>
      <th scope="col">No Order</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Nama Masakan</th>
      <th scope="col">Jumlah</th>
      <th scope="col">Status Detail Order</th>
      <th scope="col">Option</th>
    </tr>
  </thead>
  <tbody>
 
   <?php
        $no = 1;
       if(isset($_POST['filter']))
       {
         $cari = mysqli_real_escape_string($db->mysqli, $_POST['id_order']);
         $select = mysqli_query($db->mysqli, "SELECT * FROM detail_order x
          LEFT JOIN order1 z on z.id_order = x.id_order
          LEFT JOIN user b on b.id_user = z.id_user
          LEFT JOIN masakan q on q.id_masakan = x.id_masakan
          WHERE x.id_order = $cari 
          AND x.status_detail_order = 'Belum disajikan'
          AND z.status_order = 'Belum Bayar'
          ");
       }
         $query = $select or die(mysqli_error());
       ?>
    <tr>
      <?php while ($row = mysqli_fetch_array($query)) { ?>
      <th scope="row"><?php echo $no++ ?></th>
      <td><?php echo $row['nama_user']; ?></td>
      <td><?php echo $row['id_order']; ?></td>
      <td><?php echo $row['tanggal']; ?></td>
      <td><?php echo $row['nama_masakan']; ?></td>
      <td><?php echo $row['jumlah']; ?></td>
      <td><?php echo $row['status_detail_order']; ?></td>
      <td><a href="index.php?page=order&id_detail_order=<?php echo $row['id_detail_order']; ?>" class="btn btn-outline-danger">Hapus</a></td>
    </tr>
      <?php 

    }
      ?>
  </tbody>
</table>
    </div>

  
</div>
<?php
// --- Fungsi Delete
if(Input::get('id_detail_order')){
  $id_detail_order=$_GET['id_detail_order'];
  mysqli_query($db->mysqli,"DELETE FROM detail_order WHERE id_detail_order='$id_detail_order'");
// --- Tutup Fungsi Hapus
?>
<script type="text/javascript">
    alert("data berhasil dihapus")
    document.location.href="index.php?page=order"
</script>
<?php } ?>