<?php 
  require_once '../core/init.php';
  require_once 'atas.php';
$db= new koneksi();
$koneksi = $db->mysqli;
  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
 // --- Fungsi Ubah Data (Update)
function ubah($db){
    // ubah data
    if(isset($_POST['submit'])){
        $id_masakan = $_POST['id_masakan'];
        $id_paket = $_POST['id_paket'];
        $nama_masakan = $_POST['nama_masakan'];
        $harga = $_POST['harga'];
        $status_masakan = $_POST['status_masakan'];
        
        if(!empty($id_paket) && !empty($nama_masakan) && !empty($harga) && !empty($status_masakan)){
            $perubahan = "id_paket='".$id_paket."',nama_masakan='".$nama_masakan."',harga='".$harga."',status_masakan='".$status_masakan."'";
            $sql_update = "UPDATE masakan SET ".$perubahan." WHERE id_masakan=$id_masakan";
            $update = mysqli_query($db->mysqli, $sql_update);
            if($update && isset($_GET['aksi'])){
                if($_GET['aksi'] == 'update'){
                    header('location: index.php?page=edit');
                }
            }
            header('location: index.php?page=menu');
        } else {
            $pesan = "Data tidak lengkap!";
        }
    }
    
    // tampilkan form ubah
    if(isset($_GET['id_masakan'])){
        ?>
        <h4 align="center">Edit Menu Masakan</h4>
	 <form action="" class="form-group col-md-6 offset-sm-3" method="post">
	 <div class="form-group">
     <input type="hidden" name="id_masakan" value="<?php echo $_GET['id_masakan']; ?>"/>
 	</div>
    <div class="form-group">
      <label for="inputState">Paket</label>
      <select name="id_paket" id="inputState" class="form-control">
        <?php
        $select = mysqli_query($db->mysqli, "SELECT * FROM paket");
        while($data = mysqli_fetch_array($select))
      {
       ?>
        <option value="<?php echo $data['id_paket']; ?>"><?php echo $data['paket']; ?></option>
      <?php } ?>
      </select>
    </div>
    <?php
    	$id_masakan=$_GET['id_masakan'];
        $select = mysqli_query($db->mysqli, "SELECT * FROM masakan WHERE id_masakan='$id_masakan'");
        $data = mysqli_fetch_array($select);
       ?>    
  <div class="form-group">
    <label for="inputAddress">Nama Masakan</label>
    <input type="text" name="nama_masakan" minlength="1" class="form-control" id="inputEmail4" value="<?php echo $data['nama_masakan']; ?>">
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Harga</label>
      <input type="number" name="harga" minlength="1" class="form-control" id="inputEmail4" value="<?php echo $data['harga']; ?>">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Status Masakan</label>
      <select name="status_masakan" id="inputState" class="form-control">
        <option value="Ada">Ada</option>
        <option value="Tidak Ada">Tidak Ada</option>
      </select>
    </div>
  </div>
  <input type="submit" name="submit" class="btn btn-primary col-md-2" value="Ubah">
  <input type="reset" name="reset" class="btn btn-primary col-md-2" value="Reset"/>
  <p><?php echo isset($pesan) ? $pesan : "" ?></p>
</form>
        <?php
    }
}
// --- Tutup Fungsi Update
if (isset($_GET['aksi'])){
    switch($_GET['aksi']){
        case "update":
            ubah($db);
            break;
        default:
        ubah($db);
    }
} else {
    ubah($db);
}
?>