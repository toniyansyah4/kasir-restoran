<?php 
  require_once '../core/init.php';
  require_once 'atas.php';
  // require_once '../classes/koneksi.php';
  // require_once '../koneksi.php';

  $db= new koneksi();
  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  
 ?>
 <div class="container">
   <h4 align="center">Transaksi</h4>
   <form method="post" action="" align="center" class="form-group">
  <div class="form-group">
    <label for="dari">Dari</label>
    <input type="date" id="dari" name='dari'/>
    <label for="sampai">Sampai</label>
    <input type="date" id="sampai" name='sampai'/>
    <br>
    <button type="submit" class="btn btn-outline-danger" name="submit">Filter Data</button>
  </div> 

  </form>
    <div class="form-group">
      <table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Pelanggan</th>
      <th scope="col">No Order</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Pembayaran</th>
      <th scope="col">Option</th>
    </tr>
  </thead>
  <tbody>
   <?php
   if(isset($_POST['submit']))
    {
        $select2 = mysqli_query($db->mysqli, "SELECT * FROM `transaksi` a 
          LEFT JOIN user b on b.id_user=a.id_user
          WHERE tanggal BETWEEN '$_POST[dari]' AND '$_POST[sampai]' ORDER by tanggal");
     $no = 1;
        while($data1 = mysqli_fetch_array($select2))
      {
       ?>
    <tr>
      <th scope="row"><?php echo $no++ ?></th>
      <td><?php echo $data1['nama_user']; ?></td>
      <td><?php echo $data1['id_order']; ?></td>
      <td><?php echo $data1['tanggal']; ?></td>
      <td><?php echo $data1['total_bayar']; ?></td>
      <td><a href="index.php?page=tf&id_transaksi=<?php echo $data1['id_transaksi']; ?>" class="btn btn-outline-danger">Hapus</a></td>
    </tr>
      <?php }
      } ?>
  </tbody>
</table>
    </div>
</div>
<?php
// --- Fungsi Delete
if(Input::get('id_transaksi')){
  $id_transaksi=$_GET['id_transaksi'];
  mysqli_query($db->mysqli,"DELETE FROM transaksi WHERE id_transaksi='$id_transaksi'");
// --- Tutup Fungsi Hapus
?>
<script type="text/javascript">
    alert("data berhasil dihapus")
    document.location.href="index.php?page=tf"
</script>
<?php } ?>