<?php 
  require_once '../core/init.php';
  require_once 'atas.php';
  // require_once '../classes/koneksi.php';
  // require_once '../koneksi.php';

  $db= new koneksi();
  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  
 ?>
 <div class="container">
   <?php
        $select = mysqli_query($db->mysqli, "SELECT * FROM order1 a
          LEFT JOIN user b on b.id_user = a.id_user
          LEFT JOIN transaksi c on c.id_order = a.id_order
          WHERE c.total_bayar != 0
          ");
        while($data = mysqli_fetch_array($select))
      {
       ?>
        <input type="text" name="id_order" value="<?php echo $data['id_order']; ?>">
      <?php } ?>
<table class="table">
  <h4 align="center">Detail Pesanan</h4>
    <thead class="thead-light">
      <tr>
        <th>No</th>
        <th>Nama Pelanggan</th>
        <th>Barang</th>
        <th>Jumlah</th>
        <th>Harga</th>
      </tr>
    </thead>
    <tbody>
      <?php
       $no = 1;
       if(isset($_POST['filter']))
       {
         $cari = mysqli_real_escape_string($koneksi, $_POST['id_order']);
         $select = mysqli_query($db->mysqli, "SELECT * FROM detail_order c
        LEFT JOIN masakan d on d.id_masakan=c.id_masakan
        LEFT JOIN order1 e on e.id_order=c.id_order
        LEFT JOIN user z on z.id_user=e.id_user
        WHERE e.id_order = '$cari'");
       }
         $query = $select or die(mysqli_error());
        ?>
        <tr>
          <?php while ($data = mysqli_fetch_array($query)) { ?>
          <td>
            <?php echo $no++; ?>
            <input type="hidden" name="id_order" value="<?php echo $data['id_order']; ?>">
          </td>
          <td><?php echo $data['nama_user']; ?>
            <input type="text" name="nama_user" value="<?php echo $data['nama_user']; ?>">
            </td>
            <td><?php echo $data['nama_masakan']; ?>
              <input type="text" name="nama_masakan" value="<?php echo $data['nama_masakan']; ?>">
            </td>
              <td><?php echo $data['jumlah']; ?>
                <input type="text" name="jumlah" value="<?php echo $data['jumlah']; ?>">
              </td>
                  <td><?php echo "Rp. ".number_format($data['harga'], 0, ',', '.'); ?>
                  <input type="text" name="harga" value="<?php echo $data['harga']; ?>">
                  </td>
                  </tr>

                <?php } ?>
                <tr>
                  <td colspan="4">Total Harga</td>
                  <td>
                    Rp. <?php
                    if(isset($_POST['filter']))
                    {
                    $total = mysqli_query($db->mysqli, "SELECT total_bayar FROM transaksi
                      WHERE id_order = '$cari'");
                    while ($total1 = mysqli_fetch_array($total)) {
                    ?><?php echo "Rp. ".number_format($total1['total_bayar'], 0, ',', '.'); ?>
                      <?php } } ?>
                  </td>
                </tr>
              </tbody>
            </table>
</div>
<script type="text/javascript">
  window.print();
</script>