<?php 
  require_once '../core/init.php';
  require_once 'atas.php';


  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  $db= new koneksi();
 ?>
<div class="container">
  <h4 align="center">Menu</h4>
  <a href="index.php?page=input" class="btn btn-outline-primary">Input</a>
<form action="index.php?page=menu" method="post" name="clas">
  <div class="form-group" align="center">
      <label for="id_paket">Paket</label>
      <select name="id_paket" id="inputState" class="form-control">
        <?php
        $select = mysqli_query($db->mysqli, "SELECT * FROM paket");
        while($data = mysqli_fetch_array($select))
      {
       ?>
        <option value="<?php echo $data['id_paket']; ?>"><?php echo $data['paket']; ?></option>
      <?php } ?>
      </select>
      <br>
      <input type="submit" name="filter" value="Filter Data" class="btn btn-success"></input>
    </div>
  </form>

    <div class="form-group">
      <table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Masakan</th>
      <th scope="col">Harga</th>
      <th scope="col">Status Masakan</th>
      <th scope="col">Option</th>
    </tr>
  </thead>
  <tbody>
 
   <?php
        $no = 1;
       if(isset($_POST['filter']))
       {
         $cari = mysqli_real_escape_string($db->mysqli, $_POST['id_paket']);
         $select = mysqli_query($db->mysqli, "SELECT * FROM masakan WHERE id_paket = $cari ");
       }
         $query = $select or die(mysqli_error());
       ?>
    <tr>
      <?php while ($row = mysqli_fetch_array($query)) { ?>
      <th scope="row"><?php echo $no++ ?></th>
      <td><?php echo $row['nama_masakan']; ?></td>
      <td><?php echo $row['harga']; ?></td>
      <td><?php echo $row['status_masakan']; ?></td>
      <td><a href="index.php?page=edit&id_masakan=<?php echo $row['id_masakan']; ?>" class="btn btn-outline-primary">Edit</a> | <a href="index.php?page=menu&id_masakan=<?php echo $row['id_masakan']; ?>" class="btn btn-outline-danger">Hapus</a></td>
    </tr>
      <?php 

    }
      ?>
  </tbody>
</table>
    </div>

  
</div>
<?php
// --- Fungsi Delete
if(Input::get('id_masakan')){
  $id_masakan=$_GET['id_masakan'];
  mysqli_query($db->mysqli,"DELETE FROM masakan WHERE id_masakan='$id_masakan'");
// --- Tutup Fungsi Hapus
?>
<script type="text/javascript">
    alert("data berhasil dihapus")
    document.location.href="index.php?page=menu"
</script>
<?php } ?>