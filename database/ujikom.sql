-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2019 at 09:01 AM
-- Server version: 5.6.26
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `total_bayar`(IN `jumlah` DOUBLE, IN `harga` DOUBLE)
    NO SQL
SELECT SUM(jumlah * harga) FROM detail_order LEFT JOIN masakan ON detail_order.id_masakan = masakan.id_masakan$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE IF NOT EXISTS `detail_order` (
  `id_detail_order` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_masakan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status_detail_order` enum('Belum disajikan','Sudah disajikan') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1554047925 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_masakan`, `jumlah`, `keterangan`, `status_detail_order`) VALUES
(1554047918, 1554607774, 1554377088, 2, 'asd', 'Sudah disajikan'),
(1554047919, 1554607774, 1554377015, 3, 'qwe', ''),
(1554047920, 1554858854, 1554377065, 2, 'Jangan terlalu pedes', 'Sudah disajikan'),
(1554047921, 1554858854, 1554551870, 1, 'Pisah yah', 'Sudah disajikan'),
(1554047922, 1554870341, 1554377015, 1, 'qwe', 'Sudah disajikan'),
(1554047923, 1554878581, 1554377065, 2, 'Nasi ayam nya kecilin', 'Sudah disajikan'),
(1554047924, 1554878581, 1554377015, 2, 'Santannya Kurangin', 'Sudah disajikan');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'Pelanggan'),
(2, 'Admin'),
(3, 'Owner'),
(4, 'Kasir'),
(5, 'Waiter');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE IF NOT EXISTS `masakan` (
  `id_masakan` int(11) NOT NULL,
  `id_paket` int(11) NOT NULL,
  `nama_masakan` varchar(100) NOT NULL,
  `harga` double NOT NULL,
  `status_masakan` enum('Ada','Tidak Ada') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1554555521 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `id_paket`, `nama_masakan`, `harga`, `status_masakan`) VALUES
(1554377015, 2, 'Padang', 15000, 'Ada'),
(1554377033, 3, 'Nasi', 3000, 'Ada'),
(1554377065, 1, 'Grepek Ayam', 10000, 'Ada'),
(1554377088, 4, 'Bumblee Bee', 5000, 'Ada'),
(1554551870, 1, 'Nasi Ayam', 15000, 'Ada'),
(1554555520, 2, 'COMBO FIRE CHICKEN', 34000, 'Ada');

-- --------------------------------------------------------

--
-- Table structure for table `order1`
--

CREATE TABLE IF NOT EXISTS `order1` (
  `id_order` int(11) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `uang` double NOT NULL,
  `komen` text NOT NULL,
  `status_order` enum('Belum Bayar','Sudah Bayar') CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1554878582 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order1`
--

INSERT INTO `order1` (`id_order`, `no_meja`, `tanggal`, `id_user`, `uang`, `komen`, `status_order`) VALUES
(1554607774, 4, '2019-04-07 10:29:34', 1554607774, 60000, 'Sangat bagus', 'Sudah Bayar'),
(1554858854, 13, '2019-04-10 08:14:14', 1554858854, 40000, 'Bagus Waiter nya', 'Sudah Bayar'),
(1554870341, 5, '2019-04-10 11:25:42', 1554870341, 30000, '', 'Sudah Bayar'),
(1554878581, 1, '2019-04-10 13:43:01', 1554878581, 60000, '', 'Sudah Bayar');

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE IF NOT EXISTS `paket` (
  `id_paket` int(11) NOT NULL,
  `paket` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id_paket`, `paket`) VALUES
(1, 'Paket Hemat'),
(2, 'Paket Keluarga'),
(3, 'Makanan'),
(4, 'Minuman');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `total_bayar` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1554878582 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`) VALUES
(1554607774, 1554607774, 1554607774, '2019-04-07 10:29:34', 55000),
(1554858854, 1554858854, 1554858854, '2019-04-10 08:14:15', 35000),
(1554870341, 1554870341, 1554870341, '2019-04-10 11:25:42', 15000),
(1554878581, 1554878581, 1554878581, '2019-04-10 13:43:01', 50000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1554878582 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `username`, `password`, `nama_user`, `id_level`) VALUES
(117, 'toniyansyah205@gmail.com', 'Toni', '$2y$10$zAvXZ53Zv5WaXZ36HM/cIuDVTC3c72Q.ebez5lth/qce4z3CaM13K', 'Toni', 2),
(130, '', 'wayaw', '$2y$10$5m9UQQ.2Y31wL8g1tqVmn.HRSr4.io3Hc/arsdbomMtPpA0euldRC', 'Toniyansyah', 4),
(132, '', 'Toniyansyah Wahyudi', '$2y$10$Pl.Fc7Ob/HStvcKmXMaN8u8JUJyVuromsJD9WgPBI8n6atXJeP9GG', 'Toniyansyah Wahyudi', 3),
(1554551546, '', 'sandi', '$2y$10$81bUtqKPX0f6ZEmvOKeiU.0.qq6Nx6oe3SXeXh4ERY7FrdHUhYZYW', 'Sandi', 5),
(1554551551, '', 'Toniyansyah', '$2y$10$HMM4duLm7ZxM77AV50LvmuMxIy3RFuPa6daWGSsHiG5WGoZuLZ.iG', 'Toniyansyah', 3),
(1554551552, '', 'wahyudi', '$2y$10$SsYjPheSsNilqmldcrGy4e0s10kpOArsAa0gh7fEOxovTgjMCkBvS', 'Wahyudi', 5),
(1554605641, '', '', '', 'nama', 1),
(1554607774, '', '', '', 'Sandi', 1),
(1554858854, '', '', '', 'Sandi', 1),
(1554870341, '', '', '', 'Yansyah', 1),
(1554878581, '', '', '', 'Wahyudi', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id_detail_order`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_masakan` (`id_masakan`),
  ADD KEY `status_detail_order` (`status_detail_order`),
  ADD KEY `status_detail_order_2` (`status_detail_order`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `masakan`
--
ALTER TABLE `masakan`
  ADD PRIMARY KEY (`id_masakan`),
  ADD KEY `status_masakan` (`status_masakan`);

--
-- Indexes for table `order1`
--
ALTER TABLE `order1`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `tanggal` (`tanggal`),
  ADD KEY `status_order` (`status_order`),
  ADD KEY `no_meja` (`no_meja`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `tanggal` (`tanggal`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_level` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1554047925;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `masakan`
--
ALTER TABLE `masakan`
  MODIFY `id_masakan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1554555521;
--
-- AUTO_INCREMENT for table `order1`
--
ALTER TABLE `order1`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1554878582;
--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id_paket` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1554878582;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1554878582;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
