<?php 
  
  require_once '../core/init.php';

  if ( session::exists('username') ) {
    header('Location: index.php');
  }
 
  $errors = array();


  if( Input::get('submit') ) {
   
   //1. memanggil objek validasi
   $validation = new validation();

   //2. metode check
   $validation = $validation->check(array(
      'username' => array('required' => true ),
      'password' => array('required' => true )
    ));

    //3. lolos ujian
   if ( $validation->passed() ){
    if ( $user->cek_nama( Input::get('username'))) {
    if( $user->login_user( Input::get('username'), Input::get('password') ) )      
    {

     session::set('username', Input::get('username'));
         // header('Location: index.php');

       header('Location: index.php');


    }else{
      $errors[] = "login gagal";
     }
   }else{
      $errors[] = "Namanya belum terdaftar";
   }

  }else{
    $errors =  $validation->errors();
  }


  }

    require_once '../tools/header.php';
    require_once '../tools/nav_1.php';
 ?>


 <div class="container">
 	<br>
<form action="" class="form-group col-sm-6 offset-sm-3" method="post">
<?php if (!empty($errors)) { ?>
 <div class="alert alert-danger col-sm-7 offset-sm-3" role="alert" id="errors">
    <?php foreach ($errors as $error) { ?>
      <li> <?php echo "$error"; ?></li>
    <?php } ?>
  </div>
<?php } ?>
  <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Username</label>
    <div class="col-sm-10">
      <input type="text" name="username" class="form-control" id="staticEmail" placeholder="Username">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password">
    </div>
  </div>
   <input type="submit" name="submit" class="btn btn-outline-info col-md-2 offset-sm-6" value="Login">
</form>
</div>
 <?php
 	include '../tools/footer.php';
 ?>