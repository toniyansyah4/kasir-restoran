<?php 
require_once '../core/init.php';
require_once 'atas.php';

  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
$db= new koneksi();
?>
<div class="container">
	<h4 align="center">Laporan</h4>
<form method="post" action="" align="center" class="form-group">
	<div class="form-group">
		<label for="dari">Dari</label>
		<input type="date" id="dari" name='dari'/>
		<label for="sampai">Sampai</label>
		<input type="date" id="sampai" name='sampai'/>
		<br>
		<button type="submit" class="btn btn-outline-danger" name="submit">Filter Data</button>
	</div> 

	</form>
<table id="mytabel" align="center" class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Pelanggan</th>
      <th scope="col">No Order</th>
      <th scope="col">No Meja</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Nama Masakan</th>
      <th scope="col">Total</th>
      <th scope="col">Total Bayar</th>
    </tr>
  </thead>
  <tbody>
		<?php
		if(isset($_POST['submit']))
		{
			$select = mysqli_query($db->mysqli,"SELECT * FROM `detail_order` b
          LEFT JOIN transaksi c on c.id_order=b.id_order
          LEFT JOIN order1 d on d.id_order=b.id_order
          LEFT JOIN user e on e.id_user=c.id_user
          LEFT JOIN masakan f on f.id_masakan=b.id_masakan
          WHERE b.status_detail_order = 'Sudah disajikan'
          AND b.id_order = d.id_order
          AND c.tanggal BETWEEN '$_POST[dari]' AND '$_POST[sampai]' ORDER by c.tanggal");
			$no  = 1;
			while($data = mysqli_fetch_array($select)) 
			{
				?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $data['nama_user'] ?></td>
					<td><?php echo $data['id_order'] ?></td>
					<td><?php echo $data['no_meja'] ?></td>
					<td><?php echo $data['tanggal'] ?></td>
					<td><?php echo $data['nama_masakan'] ?></td>
					<td><?php echo $data['jumlah'] ?></td>
					<td><?php echo $data['total_bayar'] ?></td>
				</tr>
				<?php
			}
		}
		?>
  </tbody>
</table>
</div>