<?php 
  require_once '../core/init.php';
  require_once 'atas.php';
  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  include '../tools/header.php';
  include '../tools/nav_1.php';

 ?>


<ul class="nav justify-content-center">
  <li class="nav-item">
    <a class="nav-link active" href="index.php">Laporan</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="index.php?page=pesan">Pesanan</a>
  </li>
</ul>
    <?php
 $page = (isset($_GET['page']))? $_GET['page'] : "main";
 switch ($page) {
   case '': include "index.php"; break;
   case 'pesan': include "pesan.php"; break;
   case 'hapus': include "hapus.php"; break;
   case 'sajikan': include "sajikan.php"; break;
    case 'main' : include "main.php"; break;
 }
 ?>
<br>
 <?php include '../tools/footer.php'; ?>