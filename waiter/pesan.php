<?php 
  require_once '../core/init.php';
  require_once 'atas.php';
  // require_once '../classes/koneksi.php';
  // require_once '../koneksi.php';

  $db= new koneksi();
  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  
 ?>
 <div class="container">
  <div class="form-group">
  <h4 align="center">Pesanan</h4>
  <form action="index.php?page=pesan" method="post" class="clas">
  <div class="form-group" align="center">
      <label for="id_detail_order">No Meja dan Nama Pelanggan</label>
      <select name="id_detail_order" id="id" class="form-control">
        <?php
        $select = mysqli_query($db->mysqli, "SELECT * FROM detail_order z
          RIGHT JOIN masakan x on x.id_masakan=z.id_masakan
          RIGHT JOIN order1 a on a.id_order=z.id_order
          RIGHT JOIN user b on b.id_user=a.id_order
          WHERE z.status_detail_order = 'Belum disajikan'
          ");
        while($data = mysqli_fetch_array($select))
      {
       ?>
        <option value="<?php echo $data['id_detail_order']; ?>">No Meja :
         <?php echo $data['no_meja']; ?>
         Atas Nama : <?php echo $data['nama_user']; ?>
         Nama Masakan : 
         <?php echo $data['nama_masakan']; ?>
          </option>
      <?php } ?>
      </select>
      <br>
      <input type="submit" name="filter" value="Detail" class="btn btn-success"></input>
    </div>
  </form>
</div>
<div class="form-group">
    <form class="" action="index.php?page=sajikan" method="post" class="clas">
<table class="table" >
  <h4 align="center">Detail Pesanan</h4>
    <thead class="thead-light">
      <tr>
        <th>No</th>
        <th>Nama Pelanggan</th>
        <th>Nama Masakan</th>
        <th>Keterangan</th>
        <th>Option</th>
      </tr>
    </thead>
    <tbody>
      <?php
       $no = 1;
       if(isset($_POST['filter']))
       {
         $cari1 = mysqli_real_escape_string($db->mysqli, $_POST['id_detail_order']);
         $select = mysqli_query($db->mysqli, "SELECT * FROM detail_order c
        LEFT JOIN masakan d on d.id_masakan=c.id_masakan
        LEFT JOIN order1 e on e.id_order=c.id_order
        LEFT JOIN user z on z.id_user=e.id_user
        WHERE c.id_detail_order = '$cari1'
        AND status_detail_order = 'Belum disajikan'
        ");
       }
         $query = $select or die(mysqli_error());
        ?>
        <tr>
          <?php while ($data = mysqli_fetch_array($query)) { ?>
          <td>
            <?php echo $no++; ?>
            <input type="hidden" name="id_detail_order" value="<?php echo $data['id_detail_order']; ?>">
          </td>
          <td><?php echo $data['nama_user']; ?>
            </td>
            <td><?php echo $data['nama_masakan']; ?></td>
              <td><?php echo $data['keterangan']; ?></td>
                  <td>
                <input type="checkbox" name="status_detail_order" value="Sudah disajikan">
                Sajikan<br>
                  </td>
                  </tr>

                <?php } ?>
              </tbody>
            </table>
            <input class="btn btn-primary" name="submit" type="submit" value="Sajikan" class="validate">
</div>
</form>
</div>
</div>