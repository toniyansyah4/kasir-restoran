<?php 
  require_once '../core/init.php';
  require_once 'atas.php';
  // require_once '../classes/koneksi.php';
  // require_once '../koneksi.php';

  $db= new koneksi();
  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  
 ?>
 <div class="container">
  <div class="form-group">
  <h4 align="center">Pesanan</h4>
  <form action="index.php?page=transaksi" method="post" class="clas">
  <div class="form-group" align="center">
      <label for="id_order">No Meja dan Nama Pelanggan</label>
      <select name="id_order" id="id" class="form-control">
        <?php
        $select = mysqli_query($db->mysqli, "SELECT * FROM order1 a
          LEFT JOIN user b on b.id_user = a.id_user
          LEFT JOIN transaksi c on c.id_order = a.id_order
          WHERE a.status_order = 'Belum Bayar'
          AND c.total_bayar != 0
          ");
        while($data = mysqli_fetch_array($select))
      {
       ?>
        <option value="<?php echo $data['id_order']; ?>">No Meja :
          <?php echo $data['no_meja'];?> Atas Nama : <?php echo $data['nama_user']; ?></option>
      <?php } ?>
      </select>
      <br>
      <input type="submit" name="filter" value="Filter Data" class="btn btn-success"></input>
    </div>
  </form>
</div>
<div class="form-group">
    <form class="" action="index.php?page=bayar" method="post" class="clas">
<table class="table" >
  <h4 align="center">Detail Pesanan</h4>
    <thead class="thead-light">
      <tr>
        <th>No</th>
        <th>Nama Pelanggan</th>
        <th>Nama Masakan</th>
        <th>Jumlah</th>
        <th>Harga</th>
      </tr>
    </thead>
    <tbody>
      <?php
       $no = 1;
       if(isset($_POST['filter']))
       {
         $cari1 = mysqli_real_escape_string($db->mysqli, $_POST['id_order']);
         $select = mysqli_query($db->mysqli, "SELECT * FROM detail_order c
        LEFT JOIN masakan d on d.id_masakan=c.id_masakan
        LEFT JOIN order1 e on e.id_order=c.id_order
        LEFT JOIN user z on z.id_user=e.id_user
        WHERE e.id_order = '$cari1' ");
       }
         $query = $select or die(mysqli_error());
        ?>
        <tr>
          <?php while ($data = mysqli_fetch_array($query)) { ?>
          <td>
            <?php echo $no++; ?>
            <input type="hidden" name="id_order" value="<?php echo $data['id_order']; ?>">
            <input type="hidden" name="status_order" value="Sudah Bayar">
          </td>
          <td><?php echo $data['nama_user']; ?>
            <input type="hidden" name="nama_user" value="<?php echo $data['nama_user']; ?>">
            </td>
            <td><?php echo $data['nama_masakan']; ?></td>
              <td><?php echo $data['jumlah']; ?></td>
                  <td><?php echo "Rp. ".number_format($data['harga'], 0, ',', '.'); ?>
                  </td>
                  </tr>

                <?php } ?>
                <div class="form-group">
                <tr>
                  <td colspan="4">Total Harga</td>
                  <td>
                    Rp. <?php
                    if(isset($_POST['filter']))
                    {
                    $grand_total = mysqli_query($koneksi, "SELECT SUM(jumlah * harga) AS TOTAL FROM detail_order LEFT JOIN masakan ON detail_order.id_masakan = masakan.id_masakan 
                      WHERE detail_order.id_order = '$cari1'
                      ");
                    $rowGrandTotal = mysqli_fetch_array($grand_total);
                    echo number_format($rowGrandTotal['TOTAL'], 0, ',', '.'); ?>
                    <input type="hidden" name="total_bayar" value="<?php echo $rowGrandTotal['TOTAL']; ?>">
                      <?php } ?>
                  </td>
                </tr>
              </div>
                <tr>
                  <td colspan="4">Uang Pelanggan</td>
                  <td>
                    Rp.
                    <input type="text" name="uang" min="1" minlength="2">
                  </td>
                </tr>
              </tbody>
            </table>
            <input class="btn btn-primary" name="submit" type="submit" value="Bayar" class="validate">
            <a href="index.php?page=transaksi" class="btn btn-warning">Kembali</a>
</div>
</form>
</div>
</div>