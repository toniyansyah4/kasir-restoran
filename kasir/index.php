<?php 
  require_once '../core/init.php';
  require 'atas.php';


  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  require '../tools/header.php';
  require '../tools/nav_1.php';;
 ?>


<ul class="nav justify-content-center">
  <li class="nav-item">
    <a class="nav-link active" href="index.php">Laporan</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="index.php?page=transaksi">Transaksi</a>
  </li>
</ul>
    <?php
 $page = (isset($_GET['page']))? $_GET['page'] : "main";
 switch ($page) {
   case '': include "index.php"; break;
   case 'transaksi': include "transaksi.php"; break;
   case 'detail': include "detail.php"; break;
   case 'detail_bayar': include "detail_bayar.php"; break;
   case 'bayar': include "bayar.php"; break;
    case 'main' : include "main.php"; break;
 }
 ?>
<br>
 <?php require_once '../tools/footer.php'; ?>