<?php 
  require_once '../core/init.php';
  // require_once '../classes/koneksi.php';
  // require_once '../koneksi.php';
  if ( !session::exists('username') ) {
    header('Location: ../login/login.php');
  }
  $db= new koneksi();
  $koneksi = $db->mysqli;
  $id_order= $_SESSION['id_order'];
  $nama = $_SESSION['nama_user'];
 ?>
<div class="container">
<table id="mytabel" class="table" >
  <h4 align="center">FRESEGAR</h4>
    <thead class="thead-light">
      <tr colspan='4'>
        <h6 align="left"> Nama Kasir : <?php echo session::get('username'); ?></h6>
  <h6 align="left">Nama Konsumen : <?php echo $nama ?></h6>
  </tr>
      <tr>
        <th>No</th>
        <th>Barang</th>
        <th>Jumlah</th>
        <th>Harga</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;
      $select= "SELECT * FROM detail_order c
        LEFT JOIN masakan d on d.id_masakan=c.id_masakan
        LEFT JOIN order1 e on e.id_order=c.id_order
        LEFT JOIN transaksi q on q.id_order=e.id_order
        WHERE c.id_order = '$id_order'";
     $data1 = mysqli_query($koneksi, $select);
      while($data=mysqli_fetch_array($data1))
      {
        ?>
        <tr>
          <td>
            <?php echo $no++; ?>
          </td>
            <td>
              <?php echo $data['nama_masakan']; ?></td>
              <td>
                  <?php echo $data['jumlah']; ?></td>
                  <td>
                    <?php echo "Rp. ".number_format($data['harga'], 0, ',', '.'); ?></td>
                  </tr>

                <?php } ?>
                <tr>
                  <td colspan="3">Total Harga</td>
                  <td>
                    Rp. <?php
                    $grand_total = mysqli_query($koneksi, "SELECT SUM(jumlah * harga) AS total FROM detail_order LEFT JOIN masakan ON detail_order.id_masakan = masakan.id_masakan 
                      WHERE detail_order.id_order = '$id_order'");
                    $rowGrandTotal = mysqli_fetch_array($grand_total);
                    echo number_format($rowGrandTotal['total'], 0, ',', '.'); ?>
                  </td>
                </tr>
                <?php
                $select1 = "SELECT uang FROM order1 WHERE id_order='$id_order'";
                $data3 = mysqli_query($koneksi, $select1);
                while ($data2=mysqli_fetch_array($data3)) {
                ?>
                <tr>
                  <td colspan="3">Uang Pelanggan</td>
                    <td><?php echo "Rp. ".number_format($data2['uang'], 0, ',', '.'); ?> </td>
                </tr>
                <?php
              }
                ?>
                <tr>
                  <td colspan="3">Kembalian</td>
                  <td>
                    Rp. <?php
                    $grand_total1 = mysqli_query($koneksi, "SELECT SUM(uang - total_bayar) AS kembalian FROM order1 a
                      LEFT JOIN transaksi b on b.id_order=a.id_order 
                      WHERE a.id_order = '$id_order'");
                    $rowGrandTotal1 = mysqli_fetch_array($grand_total1);
                    echo number_format($rowGrandTotal1['kembalian'], 0, ',', '.'); ?>
                  </td>
                </tr>
              </tbody>
            </table>
            <a href="cetak.php" class="btn btn-danger" target="_BLANK">Print</a>
            <a href="index.php?page=transaksi" class="btn btn-warning"> Kembali </a>