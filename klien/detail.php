<?php
include "../tools/header.php";
include "../tools/nav.php";
?>
<?php 
  $ra = '';
  if (isset($_SESSION['nama_user']) == $ra){
    echo "<P><font color='#ff0000'><center><h1>SELAMAT DATANG</h1>";
  
    echo "<p>SILAHKAN LOGIN <a href=login_pelanggan.php>disini</a></p> ";
}else{

   ?>
<!-- pembuka menu -->
<div class="container">
  <h4 align="center">Detail Pesanan</h4>
  <?php
require_once '../core/init.php';
$db = new koneksi();
$koneksi = $db->mysqli;
?>
<form class="" action="proses_pemesanan.php" method="post">
<table class="table" >
    <thead class="thead-light">
      <tr>
        <th>No</th>
        <th>Barang</th>
        <th>Jumlah</th>
        <th>Keterangan</th>
        <th>Harga</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;
      $id_order= $_SESSION['id_order'];
      $select=mysqli_query($db->mysqli, "SELECT * FROM detail_order c
        LEFT JOIN masakan d on d.id_masakan=c.id_masakan
        LEFT JOIN order1 e on e.id_order=c.id_order
        WHERE c.id_order = '$id_order'");
      while($data=mysqli_fetch_array($select))
      {
        ?>
        <tr>
          <td>
            <input type="hidden" name="id_transaksi" value="<?php echo $_SESSION['id_transaksi']; ?>">
            <?php echo $no++; ?>
            <input type="hidden" name="nama_user" value="<?php echo $_SESSION['nama_user']; ?>">
          </td>
            <td>
              <input type="hidden" name="id_masakan" value="<?php echo $data['id_masakan']; ?>">
              <?php echo $data['nama_masakan']; ?></td>
              <td>
                  <input type="hidden" name="jumlah" value="<?php echo $data['jumlah']; ?>">
                  <?php echo $data['jumlah']; ?></td>
                  <td>
              <input type="hidden" name="keterangan" value="<?php echo $data['keterangan']; ?>">
              <?php echo $data['keterangan']; ?></td>
                  <td>
                    <input type="hidden" name="harga" value="<?php echo $data['harga']; ?>">
                    <?php echo "Rp. ".number_format($data['harga'], 0, ',', '.'); ?></td>
                  </tr>

                <?php } ?>
                <tr>
                  <td colspan="4">Total Harga</td>
                  <td>
                    Rp. <?php
                    $grand_total = mysqli_query($koneksi, "SELECT SUM(jumlah * harga) AS TOTAL FROM detail_order LEFT JOIN masakan ON detail_order.id_masakan = masakan.id_masakan 
                      WHERE detail_order.id_order = '$id_order'");
                    $rowGrandTotal = mysqli_fetch_array($grand_total);
                    echo number_format($rowGrandTotal['TOTAL'], 0, ',', '.'); ?>
                    <input type="hidden" name="total_bayar" value="<?php echo $rowGrandTotal['TOTAL']; ?>">
                  </td>
                </tr>
              </tbody>
            </table>
            <input class="btn btn-primary" name="submit" type="submit" value="Proses" class="validate">
            <a href="index.php?nama=<?php echo $_SESSION['nama_user']; ?>" class="btn btn-warning">Kembali</a>
</div>
<?php
include "../tools/footer.php";
?>
<?php 
}

?>