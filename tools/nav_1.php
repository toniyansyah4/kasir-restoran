<body>
<nav class="navbar navbar-dark bg-secondary">
	<?php 
		if ( !session::exists('username') ) {
	 ?>

	 <span class="navbar-brand mb-0 h1 col-sm-6 offset-sm-5">Welcome to Fresegar</span>
<?php }else{ ?>
	 	<a class="navbar-brand" href="#">Fresegar</a>
	 	 <span class="navbar-brand mb-0 h1"> Hi! <?php echo session::get('username'); ?></span>
  <form class="form-inline my-2 my-lg-0">
      <a href="../login/logout.php" class="btn btn-outline-light my-2 my-sm-0">Log Out</a>
    </form>
	
<!-- <ul class="nav justify-content-center">
  <li class="nav-item">
    <a class="nav-link active" href="index.php">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="register.php">Register</a>
  </li>
</ul -->
<?php } ?>
</nav>
