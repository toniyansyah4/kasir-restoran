<?php
  require_once '../core/init.php';
  $db= new koneksi();
?>
<body>
  <!-- pembuka navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
  <a class="navbar-brand" href="#">Fresegar</a>
  <div class="navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link"><?php 
        if ( isset($_SESSION['nama_user']) ) {
          echo 'Hi ' . $_SESSION['nama_user'];
         ?> <span class="sr-only">(current)</span></a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <a href="logout.php" class="btn btn-outline-light my-2 my-sm-0">Log Out</a>
    </form>
    <?php
  }else{
  echo "Masukkan Nama Anda";
  }
    ?>
  </div>
</nav>
<!-- penutup navbar -->
